﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder {

  // Public variables
  public int nodesLength;
  public List<Vector2> bestPath;
  public Vector2 nextNode;
  public List<Vector2> nodes;
  public bool connects = false;
  public float minimumDistance = Mathf.Infinity;

  // Private variables
  private GameObject chaser;
  private GameObject target;
  private LayerMask layerMask;
  private float halfWidth;
  private int pathDepth = 4;
  private float searchRadius = 20;

  public Pathfinder(
    GameObject chaser,
    GameObject target,
    LayerMask layerMask,
    int nodesLength,
    float halfWidth
  ) {
    this.chaser = chaser;
    this.target = target;
    this.layerMask = layerMask;
    this.nodesLength = nodesLength;
    this.halfWidth = halfWidth;

    bestPath = new List<Vector2>();
    GetNodes();
    GetBestPath();
  }

  void GetNodes() {
    nodes = new List<Vector2>();
    float distance = (chaser.transform.position - target.transform.position).magnitude;
    searchRadius = Mathf.Max(searchRadius, distance);
    Vector2 center = 0.5f * chaser.transform.position + 0.5f * target.transform.position;
    for (int i = 0; i < nodesLength; i++) {
      nodes.Add(center + searchRadius * Random.insideUnitCircle);
    }
  }

  public void GetBestPath () {
    Vector2 start = chaser.transform.position;
    Vector2 end = target.transform.position;
    nextNode = end;
    connects = false;
    minimumDistance = Mathf.Infinity;
    List<List<Vector2>> paths = GetPaths(pathDepth, start, end);
    foreach (List<Vector2> path in paths) {
      float distance = GetDistance(path);
      if (distance < minimumDistance) {
        bestPath = path;
        nextNode = path[1];
        connects = true;
        minimumDistance = distance;
      }
    }
  }

  List<List<Vector2>> GetPaths(int depth, Vector2 start, Vector2 end) {
    List<List<Vector2>> paths = new List<List<Vector2>>();

    bool clear = IsClear(start, end);
    if (clear) {
      List<Vector2> myPath = new List<Vector2>();
      myPath.Add(start);
      myPath.Add(end);
      paths.Add(myPath);
    } else if (depth > 0) {
      for (int i = 0; i < nodesLength; i++) {
        if (IsClear(start, nodes[i])) {
          List<List<Vector2>> tails = GetPaths(depth - 1, nodes[i], end);
          foreach (List<Vector2> tail in tails) {
            List<Vector2> myPath = new List<Vector2>();
            myPath.Add(start);
            myPath.AddRange(tail);
            paths.Add(myPath);
          }
        }
      }
    }

    return paths;
  }

  bool IsClear (params Vector2[] path) {
    for(int i = 1; i < path.Length; i++) {
      Vector2 direction = path[i] - path[i - 1];
      Vector2 perpendicular = Vector3.Cross(direction, Vector3.forward).normalized;
      Vector2 center = path[i - 1];
      Vector2 right = center + halfWidth * perpendicular;
      Vector2 left = center - halfWidth * perpendicular;

      if (
        isBlocked(center, direction) ||
        isBlocked(right, direction) ||
        isBlocked(left, direction)
      ) {
        return false;
      }
    }
    return true;
  }

  bool isBlocked (Vector2 position, Vector2 direction) {
    RaycastHit2D hit = Physics2D.Raycast(position, direction, direction.magnitude, layerMask);
    return hit.collider != null && hit.collider.gameObject != target;
  }

  float GetDistance (List<Vector2> path) {
    float distance = 0;

    for(int i = 1; i < path.Count; i++) {
      distance += (path[i] - path[i - 1]).magnitude;
    }

    return distance;
  }
}

public class Hunt : MonoBehaviour {

  // External objects
  public TeamController teamController;

  // Internal components
  private InputController inputController; 
  private Rigidbody2D body;
  private Attack attack;

  // Pathfinders
  private Pathfinder pathfinder;
  private float pathDistance;

  // Variables
  public LayerMask layerMask;
  private float halfWidth;
  private int nodesLength = 10;

  // Debugging
  private bool debug = true;
  private List<GameObject> waypoints;

	// Use this for initialization
	void Start () {
    teamController = GameObject.Find(tag).GetComponent<TeamController>();

    inputController = GetComponent<InputController>();
		body = GetComponent<Rigidbody2D>();
    attack = transform.Find("attack").GetComponent<Attack>();

    teamController.GetEnemies();
    pathfinder = GetNewPathfinder(teamController.enemies[0]);
    pathDistance = pathfinder.minimumDistance;
    GetBestPathfinder();

    halfWidth = transform.localScale.x / 2;

    int wallsMask = 1 << 8;
    int hiddenObjectsMask = 1 << 9;
    int playerMask = 1 << 10;
    layerMask = wallsMask | hiddenObjectsMask | playerMask;

    if (debug) {
      waypoints = new List<GameObject>();
      for (int i = 0; i < nodesLength; i++) {
        GameObject waypoint = Instantiate(
          (GameObject)Resources.Load("Prefabs/Waypoint", typeof(GameObject))
        );
        waypoints.Add(waypoint);
      }
    }
	}
	
	// Update is called once per frame
	void Update () {
    Turn();

    if (
      pathfinder.bestPath.Count == 2 &&
      pathfinder.minimumDistance <= 10 &&
      pathfinder.minimumDistance > attack.heldTime * 5 &&
      attack.reloadTime == 0
    ) {
      inputController.attack = true;
    } else {
      inputController.attack = false;
    }

    if (pathfinder.connects) {
      // TODO check if facing the right direction before moving forward
      inputController.up = true;
      pathfinder.GetBestPath();
      pathDistance = pathfinder.minimumDistance;
      GetBestPathfinder();
    } else {
      inputController.up = false;
      pathDistance = Mathf.Infinity;
      GetBestPathfinder();
    }

    if (debug) {
      ShowPath();
    }
	}

  Pathfinder GetNewPathfinder(GameObject target) {
    return new Pathfinder(gameObject, target, layerMask, nodesLength, halfWidth);
  }

  void GetBestPathfinder () {
    foreach (GameObject enemy in teamController.visibleEnemies) {
      Pathfinder newPathfinder = GetNewPathfinder(enemy);

      if (newPathfinder.minimumDistance < pathDistance) {
        pathfinder = newPathfinder;
        pathDistance = pathfinder.minimumDistance;
      }
    }
    // TODO Add behavior for when no enemies are found
  }

  void ShowPath () {
    for (int i = 0; i < pathfinder.nodesLength; i++) {
      waypoints[i].transform.position = pathfinder.nodes[i];
    }

    List<Vector2> path = pathfinder.bestPath;

    for (int i = 1; i < path.Count; i++) {
      // TODO combine functionality with pathfinder.isClear and TeamController.isSeenBy
      Vector2 direction = path[i] - path[i - 1];
      Vector2 perpendicular = Vector3.Cross(direction, Vector3.forward).normalized;
      Vector2 center = path[i - 1];
      Vector2 right = center + halfWidth * perpendicular;
      Vector2 left = center - halfWidth * perpendicular;

      Debug.DrawRay(center, direction, Color.red);
      Debug.DrawRay(right, direction, Color.red);
      Debug.DrawRay(left, direction, Color.red);
    }
  }

  void Turn () {
    Vector3 relativeDirection = body.GetPoint(pathfinder.nextNode);
    float turnDirection = Mathf.Atan2(relativeDirection.y, relativeDirection.x) - Mathf.PI / 2;
    turnDirection = Mathf.Sign(turnDirection);
    if (turnDirection > 0) {
      inputController.turnLeft = true;
      inputController.turnRight = false;
    } else if (turnDirection < 0) {
      inputController.turnLeft = false;
      inputController.turnRight = true;
    } else {
      inputController.turnLeft = false;
      inputController.turnRight = false;
    }
  }

  void OnDestroy () {
    if (debug) {
      for (int i = 0; i < pathfinder.nodesLength; i++) {
        Destroy(waypoints[i]);
      }   
    }
  }
}
