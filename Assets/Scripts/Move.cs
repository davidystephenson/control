﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

  private Rigidbody2D body;
  private InputController inputController;

	// Use this for initialization
	void Start () {
    body = GetComponent<Rigidbody2D>();
    inputController = GetComponent<InputController>();
	}
	
	// Update is called once per frame
	void Update () {
    if (inputController.attack) {
      Turn(3f);
    } else {
      int dx = getSign(inputController.right, inputController.left);
      int dy = getSign(inputController.up, inputController.down);
      Vector2 vector = dx * body.transform.right + dy * body.transform.up;
      body.AddForce(vector * 20);
      
      Turn(6f);
    }
	}

  int getSign (bool positive, bool negative) {
    int sign = 0;

    if (positive) {
      sign = sign + 1;
    }
    if (negative) {
      sign = sign - 1;
    }

    return sign;
  }

  void Turn (float torque) {
    int sign = getSign(inputController.turnLeft, inputController.turnRight);

    body.AddTorque(sign * torque);
  }
}
