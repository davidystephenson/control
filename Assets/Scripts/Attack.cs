﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityEngine.UI;

public class Attack : MonoBehaviour {

  // External objects and components
  private GameObject weapon;
  private GameObject indicator;
  private RectTransform indicatorRectTransform;
  private GameObject reloadTimer;
  private Text reloadTimerText;

  // Internal components
  private Rigidbody2D body;
  private InputController inputController;
  private AudioSource aimSound;
  private AudioSource fireSound;
  private AudioSource reloadSound;
  private AudioSource misfireSound;
  private AudioSource ricochetSound;

  // Public variables
  public float downTime = 0;
  public float reloadTime = 0;
  public float heldTime = 0;

  // Private variables
  private int layerMask;
  private Vector2 start;
  private Vector2 end;
  private float cooldown;
  private float fireTime;
  private bool aiming;
  private float indicatorWidth;
  private float indicatorHeight;
  private Vector2 indicatorPosition;
  private Color orange;
  private float scale;

  // Debugging
  private bool debug = false;
  private GameObject reticule0;
  private GameObject reticule1;

  // Use this for initialization
  void Start () {
    // Locate external objects
    body = transform.parent.GetComponent<Rigidbody2D>();
    inputController = transform.parent.GetComponent<InputController>();
    weapon = transform.Find("weapon").gameObject;
    aimSound = weapon.transform.Find("aimSound").GetComponent<AudioSource>();
    fireSound = weapon.transform.Find("fireSound").GetComponent<AudioSource>();
    reloadSound = weapon.transform.Find("reloadSound").GetComponent<AudioSource>();
    misfireSound = weapon.transform.Find("misfireSound").GetComponent<AudioSource>();
    ricochetSound = weapon.transform.Find("ricochetSound").GetComponent<AudioSource>();

    // Activate or deactivate debugging objects
    reticule0 = transform.Find("reticule0").gameObject;
    reticule1 = transform.Find("reticule1").gameObject;

    if (debug) {
      reticule0.SetActive(true);
      reticule1.SetActive(true);
    } else {
      reticule0.SetActive(false);
      reticule1.SetActive(false);
    }

    // Build the layer mask
    int wallsMask = 1 << 8;
    int hiddenObjectsMask = 1 << 9;
    int playerMask = 1 << 10;
    layerMask = wallsMask | hiddenObjectsMask | playerMask;

    // Save the indicator's shape and position
    if (transform.parent.Find("Canvas") != null) {
      indicator = transform.parent.Find("Canvas/indicator").gameObject;
      indicatorRectTransform = indicator.GetComponent<RectTransform>();
      reloadTimer = transform.parent.Find("Canvas/reloadTimer").gameObject;
      reloadTimerText = reloadTimer.GetComponent<Text>();
      indicatorWidth = indicatorRectTransform.rect.width;
      indicatorHeight = indicatorRectTransform.rect.height;
      indicatorPosition = indicatorRectTransform.anchoredPosition;
    }

    // Initialize variables
    cooldown = 2f;
    fireTime = -2f;
    aiming = false;
    orange = new Color(1f, 0.5f, 0f);
    scale = body.transform.localScale.x;
  }

  // Update is called once per frame
  void Update () {
    Vector2 position = body.transform.position;
    Vector2 up = body.transform.up;
    Vector2 right = body.transform.right;

    reloadTime = Mathf.Max(0, fireTime + cooldown - Time.time);
    float condensedReloadTime = Mathf.Ceil(reloadTime * 10) / 10;
    string reloadString = condensedReloadTime.ToString();
    if (reloadTime == 0) {
      reloadString = "";
    } else if (condensedReloadTime % 1 == 0) {
      reloadString = reloadString + ".0";
    }

    if (transform.parent.Find("Canvas") != null) {
      reloadTimerText.text = reloadString;

      indicatorRectTransform.sizeDelta = new Vector2(indicatorWidth * reloadTime, indicatorHeight);
      indicatorRectTransform.anchoredPosition = indicatorPosition;
    }

    if (!aiming && inputController.attack) {
      if (reloadTime == 0) {
        downTime = Time.time;
        aiming = true;
        aimSound.Play();
      } else {
        misfireSound.Play();
      }
    }

    if (aiming) {
      heldTime = Time.time - downTime;

      if (!inputController.attack) {
        // Stop aiming
        aiming = false;

        // Calculate the attack line
        start = position;
        Vector2 bodyDistance = up * 0.5f * scale;
        Vector2 weaponDistance = up * weapon.transform.localScale.y * scale;
        end = start + bodyDistance + weaponDistance + up * heldTime * 5; 

        // Check if anything was hit
        Vector2 direction = end - start;
        RaycastHit2D hit = Physics2D.Raycast(start, direction, direction.magnitude, layerMask);
        if (hit.collider != null) {
          end = hit.point;
          // TODO check enemy color instead of always red. Wait to implement this until player death has been handled.
          if (hit.collider.tag == "Red") {
            Transform hitTransform = hit.collider.transform;

            // Play the enemy's death sound
            AudioSource hitSound = hitTransform.Find("hitSound").GetComponent<AudioSource>();
            hitSound.Play();

            // Disable the enemy's attack
            hitTransform.Find("attack").GetComponent<Attack>().enabled = false;

            // Hide the enemy
            hit.collider.enabled = false;
            foreach (SpriteRenderer spriteRenderer in hit.transform.GetComponentsInChildren<SpriteRenderer>()) {
              spriteRenderer.enabled = false;
            }

            // Destroy it after the sound has finished
            Destroy(hit.collider.gameObject, hitSound.clip.length);
          } else if (hit.collider.tag == "Neutral") {
            ricochetSound.Play();
          }
        }
        fireTime = Time.time;
        fireSound.Play();
        Invoke("PlayReloadSound", cooldown - 1.265f);
        DrawLine(start, end, orange);
        heldTime = 0;
      }
    }

    if (debug) {
      DebugAttack();
    }
  }

  void DebugAttack () {
    Debug.DrawLine(start, end, Color.red);
    reticule0.transform.position = start;
    reticule1.transform.position = end;
  }

  void PlayReloadSound () {
    reloadSound.Play();
  }

  void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.1f) {
    GameObject line = new GameObject();
    line.layer = gameObject.layer;
    line.transform.position = start;
    line.AddComponent<LineRenderer>();
    LineRenderer lineRenderer = line.GetComponent<LineRenderer>();
    lineRenderer.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
    lineRenderer.startColor = color;
    lineRenderer.startWidth = 0.1f;
    lineRenderer.SetPosition(0, start);
    lineRenderer.SetPosition(1, end);
    GameObject.Destroy(line, duration);
  }
}
