﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TeamController : MonoBehaviour {
  // Public variables
  public string enemyName;
  public List<GameObject> allies;
  public List<GameObject> enemies;
  public List<GameObject> visibleEnemies;

  // Private variables
  private LayerMask layerMask;
  private GameObject enemyBase;

  // Debug
  private bool debug = true;
  private Color debugColor;

	// Use this for initialization
	void Start () {
    int wallsMask = 1 << 8;
    int hiddenObjectsMask = 1 << 9;
    int playerMask = 1 << 10;
    layerMask = wallsMask | hiddenObjectsMask | playerMask;

		if (name == "Red") {
      enemyName = "Blue";

      if (debug) {
        debugColor = Color.magenta;
      }
    } else if (name == "Blue") {
      enemyName = "Red";

      if (debug) {
        debugColor = Color.cyan;
      }
    }

    enemyBase = GameObject.Find(enemyName.ToLower() + "Base");
	}
	
	// Update is called once per frame
	void Update () {
    GetEnemies();
	}

  public void GetEnemies () {
		allies = GameObject.FindGameObjectsWithTag(name).ToList();
		enemies = GameObject.FindGameObjectsWithTag(enemyName).ToList();
    visibleEnemies = enemies.Where(enemy => allies.Any(ally => isSeenBy(enemy, ally))).ToList();

    if (!(visibleEnemies.Contains(enemyBase))) {
      visibleEnemies.Add(enemyBase);
    }
  }

  bool isSeenBy (GameObject enemy, GameObject ally) {
    Vector2 direction = ally.transform.position - enemy.transform.position;

    Dictionary<string, Vector2> positions = getPositions(direction, enemy);

    bool clear = (
      isClear(ally, positions["center"], enemy) ||
      isClear(ally, positions["right"], enemy) ||
      isClear(ally, positions["left"], enemy)
    );

    if (debug && clear) {
      Debug.DrawLine(positions["right"], positions["left"], debugColor);
    }

    return clear;
  }

  bool isClear(GameObject ally, Vector3 target, GameObject enemy) {
    Vector2 direction = target - ally.transform.position;
    RaycastHit2D hit = Physics2D.Raycast(
      ally.transform.position, direction, direction.magnitude, layerMask
    );
    bool clear = hit.collider == null || hit.collider.gameObject == enemy;

    if (debug) {
      if (clear) {
        Debug.DrawRay(ally.transform.position, direction, debugColor);
      } else {
        Debug.DrawRay(ally.transform.position, direction, Color.gray);
      }
    }

    return clear;
  }

  Dictionary<string, Vector2> getPositions (Vector2 direction, GameObject target) {
    Vector3 perpendicular = Vector3.Cross(direction, Vector3.forward).normalized;

    float halfWidth = target.transform.localScale.x / 2;
    
    Dictionary<string, Vector2> positions = new Dictionary<string, Vector2>();
    positions.Add("center", target.transform.position);
    positions.Add("right", target.transform.position + halfWidth * perpendicular);
    positions.Add("left", target.transform.position - halfWidth * perpendicular);

    return positions;
  }
}
